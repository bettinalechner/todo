<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'GroupController@index');

Route::resource('groups', 'GroupController');

Route::patch('groups/{groups}/todos/{todos}/toggle-completed', 'ToDoController@toggleCompleted');
Route::resource('groups.todos', 'ToDoController', ['only' => ['create', 'store', 'edit', 'update']]);
Route::resource('todos', 'ToDoController', ['except' => ['create', 'store', 'edit', 'update']]);