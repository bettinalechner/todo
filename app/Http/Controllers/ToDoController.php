<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ToDo;
use App\Group;

class ToDoController extends Controller
{
    public function index()
    {
        $todos = ToDo::all();
        return view('todos.index', compact('todos'));
    }

    public function show($id)
    {
        $todo = ToDo::findOrFail($id);
        return view('todos.show', compact('todo'));
    }

    public function create($groupId)
    {
        return view('todos.create', compact('groupId'));
    }

    public function store(Requests\ToDoRequest $request, $groupId)
    {
        $group = Group::findOrFail($groupId);
        $todo = ToDo::create($request->all());
        $group->toDos()->save($todo);

        return redirect()->route('groups.show', $group->id);
    }

    public function edit($groupId, $id)
    {
        $todo = ToDo::findOrFail($id);
        $groups = Group::orderBy('name')->lists('name', 'id')->toArray();
        return view('todos.edit', compact('todo', 'groups'));
    }

    public function update(Requests\ToDoRequest $request, $groupId, $id)
    {
        $todo = ToDo::findOrFail($id);
        $todo->update($request->all());
        return redirect()->route('groups.show', ['id' => $todo->group->id]);
    }

    public function toggleCompleted(Request $request, $groupId, $id)
    {
        $group = Group::findOrFail($groupId);
        $todo = ToDo::findOrFail($id);
        $todo->completed = $request->completed;
        $todo->save();
        return redirect()->route('groups.show', $group->id);
    }

    public function destroy($id)
    {
        $todo = ToDo::findOrFail($id);
        $groupId = $todo->group->id;
        $todo->delete();
        return redirect()->route('groups.show', $groupId);
    }
}
