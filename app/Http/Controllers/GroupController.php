<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Group;

class GroupController extends Controller
{
    public function index()
    {
        $groups = Group::all();
        return view('groups.index', compact('groups'));
    }

    public function show($id)
    {
        $group = Group::findOrFail($id);
        return view('groups.show', compact('group'));
    }

    public function create()
    {
        return view('groups.create');
    }

    public function store(Requests\GroupRequest $request)
    {
        $group = Group::create($request->all());
        return redirect()->route('groups.show', $group->id);
    }

    public function edit($id)
    {
        $group = Group::findOrFail($id);
        return view('groups.edit', compact('group'));
    }

    public function update(Requests\GroupRequest $request, $id)
    {
        $group = Group::findOrFail($id);
        $group->update($request->all());
        return redirect()->route('groups.show', ['id' => $group->id]);
    }

    public function destroy($id)
    {
        $group = Group::findOrFail($id);
        $group->delete();
        return redirect()->route('groups.index');
    }
}
