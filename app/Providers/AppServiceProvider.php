<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Group;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('groups._group_list', function ($view) {
            $view->with('groups', Group::orderBy('name')->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
