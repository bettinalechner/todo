<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDo extends Model
{
    protected $fillable = [
        'task',
        'due_date',
        'completed',
        'group_id',
    ];

    public function group()
    {
        return $this->belongsTo('App\Group');
    }
}
