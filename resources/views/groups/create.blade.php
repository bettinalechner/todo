@extends('app')

@section('content')

<h1>Add a new Group</h1>

{!! Form::open(['action' => 'GroupController@store']) !!}
    @include('groups.form')
{!! Form::close() !!}

@endsection