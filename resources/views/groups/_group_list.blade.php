<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="{{ action('GroupController@create') }}" class="btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span></a>
            <h1 class="panel-title">Groups</h1>
        </div>
        <ul class="list-group">
            @foreach($groups as $group)
                <li class="list-group-item">
                    <a href="{{ action('GroupController@show', [$group->id]) }}">{{ $group->name }}</a>
                    ({{ $group->toDos->count() }} Tasks)
                    <a href="{{ action('GroupController@destroy', [$group->id]) }}" class="btn btn-danger btn-xs pull-right"><span class="glyphicon glyphicon-trash"></span></a>
                    <a href="{{ action('GroupController@edit', [$group->id]) }}" class="btn btn-primary btn-xs pull-right"><span class="glyphicon glyphicon-pencil"></span></a>
                </li>
            @endforeach
        </ul>
    </div>
</div>