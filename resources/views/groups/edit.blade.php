@extends('app')

@section('content')

<h1>Edit {{ $group->name }}</h1>

{!! Form::model($group, ['action' => ['GroupController@update', $group->id], 'method' => 'PATCH']) !!}
    @include('groups.form')
{!! Form::close() !!}

@endsection