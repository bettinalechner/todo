@extends('app')

@section('content')

<h1>To-Dos</h1>

<p><a href="{{ action('ToDoController@create') }}" class="btn btn-primary">Add new To-Do</a></p>

<table class="table">
    <thead>
        <tr>
            <td>Task</td>
            <td>Group</td>
            <td>Due Date</td>
            <td>Completed</td>
            <td></td>
        </tr>
    </thead>
    <tbody>
        @foreach($todos as $todo)
            <tr>
                <td><a href="{{ action('ToDoController@show', [$todo->id]) }}">{{ $todo->task }}</a></td>
                <td><a href="{{ action('GroupController@show', [$todo->group->id]) }}">{{ $todo->group->name }}</a></td>
                <td>{{ $todo->due_date }}</td>
                <td>{{ $todo->completed }}</td>
                <td>
                    <a href="{{ action('ToDoController@edit', [$todo->id]) }}" class="btn btn-primary">Edit</a>
                    <a href="{{ action('ToDoController@destroy', [$todo->id]) }}" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection