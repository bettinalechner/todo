@extends('app')

@section('content')

<h1>Edit {{ $todo->name }}</h1>

{!! Form::model($todo, ['action' => ['ToDoController@update', $todo->group->id, $todo->id], 'method' => 'PATCH']) !!}
    @include('todos.form')
{!! Form::close() !!}

@endsection