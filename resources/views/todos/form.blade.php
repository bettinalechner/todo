<div class="row">
    <div class="col-md-3">
        {!! Form::label('task') !!}
        {!! Form::text('task', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-3">
        {!! Form::label('due_date') !!}
        {!! Form::date('due_date', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    </div>
</div>